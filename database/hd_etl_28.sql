-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 14 Sty 2017, 02:28
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hd`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cms_users`
--

CREATE TABLE IF NOT EXISTS `cms_users` (
  `usr_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_email` varchar(245) NOT NULL,
  `usr_password` varchar(245) NOT NULL,
  `usr_name` varchar(245) DEFAULT NULL,
  `usr_lastname` varchar(245) DEFAULT NULL,
  `usr_type` enum('ROOT','WORKER') DEFAULT 'WORKER',
  `usr_createtime` datetime DEFAULT NULL,
  `usr_active` tinyint(1) DEFAULT '1',
  `usr_hash` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `usr_email_UNIQUE` (`usr_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `cms_users`
--

INSERT INTO `cms_users` (`usr_id`, `usr_email`, `usr_password`, `usr_name`, `usr_lastname`, `usr_type`, `usr_createtime`, `usr_active`, `usr_hash`) VALUES
(2, 'etl@etl.pl', '52e5af581e61707bfde3edf58cddc44e0bffdc45', 'test', 'test', 'ROOT', '2016-01-06 00:00:00', 1, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `opinions`
--

CREATE TABLE IF NOT EXISTS `opinions` (
  `opn_id` int(11) NOT NULL AUTO_INCREMENT,
  `text` longtext,
  `date` datetime DEFAULT NULL,
  `author` varchar(255) DEFAULT '',
  `possesion` tinyint(1) DEFAULT NULL,
  `rate` decimal(4,2) DEFAULT NULL,
  `recommend` varchar(255) DEFAULT '',
  `advantage` longtext,
  `disadvantage` longtext,
  `vote_yes` int(4) DEFAULT NULL,
  `vote_no` int(4) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `prd_id` int(11) NOT NULL,
  PRIMARY KEY (`opn_id`),
  KEY `key` (`comment_id`),
  KEY `opn_idx` (`prd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1185 ;

--
-- Zrzut danych tabeli `opinions`
--

INSERT INTO `opinions` (`opn_id`, `text`, `date`, `author`, `possesion`, `rate`, `recommend`, `advantage`, `disadvantage`, `vote_yes`, `vote_no`, `comment_id`, `prd_id`) VALUES
(1063, 'b. zadowolony z zakupu', '2016-12-23 15:00:50', 'iwanracing', 0, '5.00', 'Polecam', NULL, NULL, 1, 0, 4305343, 47053736),
(1064, 'Mam go od 28 listopada to mój 3 zegarek i tego nie oddam nikomu. Świetny', '2016-12-18 11:03:13', 'WDT', 0, '5.00', 'Polecam', NULL, NULL, 1, 0, 4293437, 47053736),
(1065, 'Z Samsungiem S5 neo działa bez zarzutów. Serdecznie polecam', '2016-12-14 19:54:40', 'Arek', 0, '5.00', 'Polecam', NULL, NULL, 1, 0, 4286718, 47053736),
(1066, 'Regres względem pierwszego Gear S który miał slot na kartę sim. Brak autonomii. Wymagany smartfon z androidem najlepiej samsunga i z 2GB ram inaczej są problemy. Gorsze ppi niż w S2. LTE niedziałające w kraju. Ceny wyższe niż producenta. Trudno dostępny. Olbrzymi jak na kobiecy lub mały  nadgarstek.', '2017-01-08 21:25:50', 'mms', 0, '3.00', '', NULL, NULL, 0, 0, 4337460, 47053736),
(1121, 'Dobrze trzema sie w rece, nowy kolor jest ok, ogolnie duzych zmian w stosunku do 6s nie widze.', '2016-09-27 07:52:55', 'Daro', 0, '4.50', 'Polecam', NULL, NULL, 5, 3, 3986416, 47044601),
(1122, 'nie ocenie bo produkt do mnie nie dotarł', '2016-12-27 13:25:01', 'ArtG', 1, '0.50', 'Nie polecam', NULL, NULL, 0, 3, 4311965, 47044601),
(1123, 'Apple iPhone 7 32GB Czarny', '2016-10-07 10:06:58', 'Kuba', 1, '5.00', 'Polecam', NULL, NULL, 0, 3, 4017230, 47044601),
(1124, 'Mój pierwszy iphone i spełnia moje oczekiwania.', '2016-12-05 07:52:43', 'KatPaw', 1, '5.00', 'Polecam', NULL, NULL, 1, 0, 4258269, 47044601),
(1125, 'Coś pięknego!!!', '2016-11-08 21:18:45', 'Bartosz', 1, '5.00', 'Polecam', NULL, NULL, 1, 0, 4167064, 47044601),
(1126, 'Produkt Praktycznie bez wad najlepszy obecnie na rynku', '2016-10-28 19:01:55', 'Jarek', 1, '4.50', 'Polecam', NULL, NULL, 1, 0, 4118630, 47044601),
(1127, 'Bardzo dobry wygląd smartfona. Kilka razy potrafił się zaciąć, ale jest to sporadyczne. System banalny w obsłudze.', '2016-12-30 11:45:18', 'Julia', 1, '4.50', 'Polecam', NULL, NULL, 0, 1, 4319628, 47044601),
(1128, 'bdb', '2016-12-22 18:13:39', 'michal', 1, '4.00', 'Polecam', NULL, NULL, 0, 1, 4303808, 47044601),
(1129, 'Szybki, funkcjonalny.', '2016-12-21 22:14:03', 'towarzysz.tom', 1, '4.50', 'Polecam', NULL, NULL, 0, 1, 4302108, 47044601),
(1130, 'Nic dodać nic ująć, marka mówi sama za siebie', '2017-01-07 20:47:47', 'Użytkownik Ceneo', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4336162, 47044601),
(1131, 'Super gadżet', '2016-12-28 12:23:26', 'Monika', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4314737, 47044601),
(1132, 'super. polecam', '2016-12-27 08:14:38', 'Janusz', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4310646, 47044601),
(1133, 'Swietny telefon', '2016-12-23 16:13:25', 'Elyan', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4305456, 47044601),
(1134, 'Świetny telefon:)', '2016-12-14 16:58:38', 'Ilona', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4286394, 47044601),
(1135, 'Po prostu Iphone.:))', '2016-12-09 11:05:07', 'Mariusz', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4272711, 47044601),
(1136, 'supeeeer', '2016-11-26 21:52:35', 'cenrom', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4236798, 47044601),
(1137, 'Świetny produkt!', '2016-11-21 16:23:43', 'Marcin', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4221706, 47044601),
(1138, 'Bardzo dobry.', '2016-11-09 04:53:23', 'MS', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4167787, 47044601),
(1139, 'Polecam', '2016-11-03 15:45:19', 'Adam', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4147328, 47044601),
(1140, 'Doskonały telefon', '2016-10-19 20:48:14', 'Dorota', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4074344, 47044601),
(1141, 'Polecam !!! kocham H.J <3', '2016-10-09 02:29:26', 'Mateusz', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4024816, 47044601),
(1142, 'Dosłownie przypadkiem udało mi się dorwać i od razu kupić.  <br/>  <br/> Ogólnie to dopiero mój drugi iPhone. Po przejściu z 6 widzę wyraźny skok w szybkości urządzenia (uważam, że z 6s się nie opłaca przesiadać, ale jeśli ktoś posiada 6 i niższe, to zdecydowanie warto). <br/>  <br/> Ogólnie aż tak wielu zmian nie widzę...: <br/> 1. Głośniki stereo? - Fakt, są. Grają nieźle, różnica jest. Ale dla mnie nie jakaś znacząca choć fakt faktem dobrze że w końcu postawili na głośniki stereo. <br/> 2. Brak mini jacka? - Dla mnie osobiście nie robi to probemu, gdyż muzyki z telefonu nie słucham. Dla wielu osób jednak może być to problemem i dla mnie to mimo wszystko trochę śmieszne zabierać wejście na słuchawki i dodawać przejściówkę. Co prawda zabranie wejścia na słuchawki miało na celu poprawienie 3D touch i dodanie tego w przycisku domowym, ale właśnie... <br/> 3. Przycisk domowy zmieniony na technologię 3D touch - jak na razie mam mieszane uczucia. Działa dobrze, ale jednak nie jest fizyczny i na razie to odczuwam. Działa tak samo jak normalny fizyczny, więc to pewnie kwestia przyzwyczajenia. Dodatkowo czytnik linii papilarnych w porównaniu do 6 poprawiony i widać sporą różnicę.  <br/> 4. Samo 3D touch to też dla mnie nowość - działać działa, opcje są fajne i sporo upraszczają, ale na razie i tak odruchowo korzystam standarowo. Jednak kilka godzin z urządzeniem, to za mało by się przestawić (ale wystarczająco by dać opinię, bo to jednak iPhone i wiele rzeczy pozostaje bez zmian). <br/> 5. Widzę już teraz zdecydowanie lepszą baterię, ale może to być spowodowane "zajechaniem" poprzedniego telefonu, więc trudno stwierdzić jak to się ma w  praktyce. <br/> 6. Aparat rzeczywiście znacząco poprawiony i widać sporą różnicę w porównaniu do IP6, także tutaj na plus.  <br/> 7. Kwestia wodoodporności/wodoszczelności - IP67. Tutaj jest dość śmieszna kwestia. Co prawda jeszcze nie testowane, ale szczerze nie wiem czy testować, skoro nawet w sklepie przy kupnie uprzedzają, że to nie jest tak na sto procent i raczej nie polecają na chama kąpać się z telefonem. Lekka ironia jednak... <br/>  <br/> Ogólnie kupna nie żałuję, od dawna zależało mi by wymienić mojego IP6 na szybszy model. Z uwagi, że to dopiero mój drugi iPhone zaskoczyło mnie pierwszy raz pozytywnie iTunes. Nienawidzę tego badziewia od początku posiadania IP, ale jeśli chodzi o wymianę urządzenia na nowe...? To najlepsze co mi się trafiło! Wystarczyło zrobić kopię zapasową i przy konfiguracji sprzętu zaznaczyć by odtworzył z iTunes wszystko - moje oczekiwania oscylowały głównie na tym, że skopiuje mi zdjęcia, muzykę i ewentualnie inne mało znaczące pierdoły. Ale to, że skopiował wszystkie sms, kontakty ze zdjęciami, a nawet takie rzeczy jak ustawienia poprzednie, wszystkie ustawienia budzika, tapetę, dzwonki i zainstalował sam wszystkie aplikacje, które były poprzednio (nie było trzeba się nigdzie logować na nowo, a we wszystkich grach zostały nawet zapisane poziomy), a nawet poustawiał wszystko w tej samej kolejności, to dla mnie nowość - zapewne wieloletni użytkownicy IP znają to od dawna, ale dla mnie to bardzo miła niespodzianka. Praktycznie dwie godziny ustawiania telefonu pod siebie mam z głowy, bo.... jest ustawiony tak samo jak wcześniej. Jak to można się cieszyć z małych rzeczy...  <br/>  <br/> Tak więc jak już udało mi się wspomnieć wcześniej - jeśli ktoś posiada modele niższe niż IP 6s i ma pieniądze i chęć zakupić najnowszego ogryzka, to moim zdaniem zdecydowanie warto. Skok wydajnościowy jest bardzo widoczny i widać ogólnie sporą różnicę w działaniu samego urządzenia, co było dla mnie priorytetowe, a przy tym dostajemy parę nowinek. Przy IP 6s myślę, że różnicy można praktycznie żadnej nie zauważyć jeśli się jest standardowym użytkownikiem, także uważam, że w tym wypadku nie warto wymieniać.', '2016-09-24 00:27:29', 'fryzzztyyykk', 0, '5.00', 'Polecam', NULL, NULL, 31, 2, 3978595, 47044601),
(1143, 'Jest ok', '2016-09-18 21:16:19', 'Krzysztof', 0, '5.00', 'Polecam', NULL, NULL, 0, 16, 3963070, 47044601),
(1144, 'Czy tu ten telefon przychodzi?  <br/> Bo juz kiedys mnie oszukali na innej strone', '2016-12-04 19:24:04', 'Ewka69.69', 0, '1.00', 'Nie polecam', NULL, NULL, 0, 2, 4256839, 47044601),
(1145, 'ŚWIETNY smartfon przesiadka z iP 5C mega polecam Apple uzależnia!!!:) <br/>', '2016-12-02 21:42:42', 'ZŁOTY', 0, '5.00', 'Polecam', NULL, NULL, 1, 0, 4252385, 47044601),
(1146, 'Genialny smartphone! Bateria starcza na cały dzień a ekran jest w sam raz dla mnie!', '2016-12-04 00:29:46', 'Użytkownik Ceneo', 0, '1.00', 'Nie polecam', NULL, NULL, 0, 1, 4254604, 47044601),
(1147, 'Pozytywnie zaskoczył mnie czas pracy na  <br/> baterii po powrocie do Androida po przesiadce  <br/> z iPhone''a. Sam telefon dopiero testuję, więc  <br/> trudno coś o nim powiedzieć więcej na razie  <br/> ale jest OK <br/> na plus aparat i czytnik lini papilarnych', '2016-11-23 22:03:27', 'Użytkownik Ceneo', 0, '4.50', 'Polecam', NULL, NULL, 0, 1, 4230008, 47044601),
(1148, 'Może nie innowacyjny ale nadal trzymający poziom marki', '2016-11-18 17:10:04', 'Bartek', 0, '4.50', 'Polecam', NULL, NULL, 0, 1, 4212476, 47044601),
(1149, 'Super', '2016-11-09 09:19:10', 'Centrum Bizneso', 0, '4.50', 'Polecam', NULL, NULL, 0, 1, 4168162, 47044601),
(1150, 'spoko', '2016-10-12 19:43:16', 'RafU', 0, '4.50', 'Polecam', NULL, NULL, 0, 1, 4041671, 47044601),
(1151, 'Produkt sam w sobie jest OK. Ma swoje ograniczenia, ale można to jeszcze wytrzymać ;)', '2016-09-29 11:22:36', 'Grzegorz', 0, '4.00', 'Polecam', 'funkcjonalność<br /> intuicyjność<br /> wygląd', 'cena<br /> czas pracy na baterii', 0, 1, 3993360, 47044601),
(1152, 'Polecam. Chociaż cena jest absurdalna!', '2017-01-13 16:14:01', 'Nicholson', 0, '5.00', 'Polecam', 'jakość wykonania<br /> system', NULL, 0, 0, 4347674, 47044601),
(1153, 'nie zawiesza sie ,nie zrywa połączeń,jest bardzo szybki,zdjęcia robi bardzo dobrej jakości. Bardzo polecam!', '2017-01-09 16:28:00', 'steppannovva', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4339177, 47044601),
(1154, 'Super przesiadłem sie z iP5c Bateria na luzie 1 dzien czarny mat swietnie wyglada aparaty ok. Jeśli używasz sprzetu od Apple to bedziesz zadowolony. Brak minijacka mi nie przeszkazda jest przejsciowka albo poprostu sluchawki bezprzewodowe. glosniki graja glosno telefon sie nie grzeje. Cena troche wyskowa wiadomo Apple. Goraco polecam nie pożałujesz  zarowno urzytkowniku Apple jak i przesiadka z andreoidaaa', '2017-01-04 22:43:10', 'zloty', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4330939, 47044601),
(1155, 'Bardzo funkcjonalny telefon, działa szybko i płynnie, a jego cena nie jest za wysoka. Gorąco polecam!', '2017-01-01 13:49:35', 'Użytkownik Ceneo', 0, '4.00', 'Polecam', NULL, NULL, 0, 0, 4322840, 47044601),
(1156, 'UWAGA! Ten sprzedawca to oszust i wysyla nie oryginalne telefony!!', '2016-12-29 12:20:33', 'Popi', 0, '0.50', 'Nie polecam', NULL, NULL, 0, 0, 4317292, 47044601),
(1157, 'Polecam,jedyny minus to to,ze od momentu kupienia (ok.poltora miesiąca temu) musiałem go restartowac już 2 razy,ponieważ się zaciął', '2016-12-27 19:31:35', 'Adam', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4313073, 47044601),
(1158, 'Najlepszy telefon na świecie ;) zwłaszcza do pracy.', '2016-12-18 20:43:03', 'Michal', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4295016, 47044601),
(1159, 'Nowy home button. coś w czym się zakochacie po przesiadce ze starszego modelu.. 5/5.', '2016-12-17 07:16:11', 'Marcel', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4291555, 47044601),
(1160, 'ok', '2016-12-16 04:55:30', 'Użytkownik Ceneo', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4289652, 47044601),
(1161, 'Na 6', '2016-12-09 21:37:03', 'f16', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4274948, 47044601),
(1162, 'Najlepszy sprzęt jaki kiedykolwiek używałem, a używałem wszystkie obecne flagowce. I tyle', '2016-11-28 21:06:51', 'nikt', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4242419, 47044601),
(1163, 'najlepsza marka na swiecie , mercedes wsrod telefonow , musi byc drogi bo wtedy chce sie go miec , marka legenda analogocznie do MB marka legenda', '2016-11-28 10:14:49', 'dsdsd', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4240468, 47044601),
(1164, 'FAJNE', '2016-11-24 17:52:11', 'Sandeep', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4231669, 47044601),
(1165, 'Polecam', '2016-11-21 09:00:36', 'miodo', 0, '4.00', 'Polecam', NULL, NULL, 0, 0, 4219891, 47044601),
(1166, 'Mam go od dwóch tygodni (Iphone 7 BLACK 128GB) kupiłem w sklepie Apple, bo nigdzie nie mogłem dostać. Wcześniej miałem Nokia Lumie 1020. <br/> To najlepszy telefon jaki miałem, wszystkie "szajsungi" się chowają, telefon działa rewelacyjnie, zdjęcia robi bardzo ładne (ładniejsze niż moja lumia 1020) Przycisk 3d touch bardzo mi się podoba, działa fajniej niż w 6s (żona ma to mam porównanie). Dałem za niego 3763zł i nie żałuje. Jestem bardzo zadowolony, polecam, wszystkim', '2016-11-19 19:52:48', 'Marcin', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4216271, 47044601),
(1167, 'Super, nie ma lepszego telefonu.', '2016-11-19 18:49:07', 'Rafał', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4216144, 47044601),
(1168, 'OK', '2016-11-17 15:37:37', 'Dario', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4206588, 47044601),
(1169, 'Piękny, szybki i niezawodny smartfon. Jeśli komuś nie żal pieniędzy na wspaniały telefon to godny zakupu ! Polecam :)', '2016-11-06 20:49:03', 'Kamil', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4158772, 47044601),
(1170, 'APPLE IPHONE 7 32GB.Bardzo dobre parametry techniczne. Dobra cena. Sprzęt warty i godny jak najbardziej polecenia.', '2016-10-30 21:32:51', 'Użytkownik Ceneo', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4129533, 47044601),
(1171, 'Telefon PETARDA! POLECAM KAŻDEMU', '2016-10-27 17:35:55', 'l', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4112503, 47044601),
(1172, 'Znakomity produkt świetne wykonanie i bardzo dobra cena. Gorąco polecam wszystkim zainteresowanym', '2016-10-23 12:15:20', 'Piotr', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4090749, 47044601),
(1173, 'Cudo', '2016-10-11 13:23:09', 'Ania', 0, '5.00', 'Polecam', NULL, NULL, 0, 0, 4034495, 47044601),
(1174, 'Polecam', '2016-10-03 15:10:02', 'KAPI', 0, '5.00', 'Polecam', 'funkcjonalność<br /> intuicyjność<br /> wodoodporność', 'cena', 0, 0, 4003660, 47044601),
(1175, 'Super sprzęt.', '2017-01-12 16:40:41', 'Domi', 1, '5.00', 'Polecam', NULL, NULL, 0, 0, 4345731, 47053736),
(1176, 'Właśnie go odebrałem,  <br/> Nie mogę znaleźć messengera, facebooka.  <br/> Zobaczymy', '2017-01-11 23:31:31', 'Użytkownik Ceneo', 1, '4.50', 'Polecam', NULL, NULL, 0, 0, 4344465, 47053736),
(1177, 'Zegarek nie działa ze wszystkimi smartfonami z androidem 4,4 bądź wyższym jak jest napisane w opisie u większości sprzedawców. Trzeba zaznaczyc jeszcze ze potrzebne jest minimum 1,5GB RAM w telefonie.Jest oficjalna lista urządzeń kompatybilnych z zegarkiem (oprocz telefonow samsunga oczywiscie):', '2016-12-13 17:20:17', 'Dario', 0, '4.00', 'Polecam', NULL, NULL, 4, 0, 4284076, 47053736),
(1178, 'Tylko nie kupujcie w sklepach SAMSUNG BRAND STORE sa poważne problemy z reklamacjami', '2016-12-07 10:47:13', 'Marcin', 0, '0.50', 'Nie polecam', NULL, NULL, 3, 3, 4264421, 47053736),
(1179, 'Polecam. Super materiały użyte w zegarku. Dopracowane funkcje fit. Mega wyraźny ekran. Bardzo dobra bateria. Dużo tarcz zegarków. Bardzo szybki i wymienne paski. Wszystko na tak nie ma do czego się przyczepić. Jeżeli już to poprzednio miałem gear S i szkoda wbudowanej kamerki.', '2016-12-16 16:03:54', 'Maciek', 0, '5.00', 'Polecam', NULL, NULL, 2, 1, 4290828, 47053736),
(1180, 'Brak wejscia na karte sim i co za tym idzie brak samodzielnego wykonywania połączeń i wysyłania smsow. <br/> Pierwszy gear S ma tą możliwosc wykonywania polaczen samodzielnie bez uzycia smartfona.  <br/> Samsung sie powoli cofa szkoda ze cena juz sie nie cofa. <br/>', '2016-12-18 08:59:47', 'Gismo', 0, '1.00', 'Nie polecam', NULL, NULL, 1, 2, 4293231, 47053736),
(1181, 'Spoko', '2016-12-11 03:23:09', 'Xd', 0, '5.00', 'Polecam', NULL, NULL, 1, 2, 4277303, 47053736),
(1182, 'To co wyróżnia zegarek Samsung Gear S3 na tle konkurencji to bardzo duża energooszczędność oraz ekran SUPER AMOLED. Dużym udogodnieniem jest też możliwość wymiany paska na jaki tylko nam się podoba. Może to być nawet elegancka srebrna bransoleta. To co jest ważne to  norma wodoodporności IP68. Smartwatch może być zanurzony do półtora metra głębokości na pół godziny. Jest to objęte gwarancją.', '2016-12-16 11:04:01', 'Ambasador marki Samsung', 0, '5.00', 'Polecam', NULL, NULL, 1, 1, 4290063, 47053736),
(1183, 'Nigdzie nie można go zobaczyć na żywo. Wszędzie teoretycznie jest a jak się chce kupić to niedostępny... samsung brawo.', '2016-12-17 13:03:42', 'Ja', 0, '0.50', 'Nie polecam', NULL, NULL, 0, 3, 4291994, 47053736),
(1184, 'emag.pl sztucznie winduje cenę tego produktu jeszcze przed 2 h cena była "standardowa" 1698 zł tymczasem po 2 h już 1779 zł wiedząc zę obecnie nigdzie nie jest dostepny. chytrość nie zna granic', '2016-12-29 17:46:42', 'Michał', 0, '0.50', 'Nie polecam', NULL, NULL, 1, 0, 4318200, 47053736);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `opt_id` int(11) NOT NULL AUTO_INCREMENT,
  `opt_name` varchar(245) NOT NULL DEFAULT '',
  `opt_value` text,
  `opt_namespace` varchar(245) NOT NULL,
  `opt_image` tinyint(1) NOT NULL DEFAULT '0',
  `opt_key` varchar(245) NOT NULL,
  PRIMARY KEY (`opt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Zrzut danych tabeli `options`
--

INSERT INTO `options` (`opt_id`, `opt_name`, `opt_value`, `opt_namespace`, `opt_image`, `opt_key`) VALUES
(1, 'Tytuł Strony', 'HD ETL', 'page', 0, 'page_title'),
(3, 'Użytkownik e-mail', 'etl@etl.pl', 'mail', 0, 'mail_username'),
(4, 'Hasło e-mail', 'etl', 'mail', 0, 'mail_password'),
(5, 'Serwer smtp', 'etl@etl.pl', 'mail', 0, 'mail_smtp'),
(6, 'Słowa Kluczowe', 'ETL', 'page', 0, 'page_keywords'),
(7, 'Opis strony', 'Opis strony', 'page', 0, 'page_description');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `prd_id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `specs` longtext,
  PRIMARY KEY (`prd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `products`
--

INSERT INTO `products` (`prd_id`, `type`, `brand`, `model`, `name`, `specs`) VALUES
(47044601, 'Smartfony', 'Apple', 'iPhone 7 32GB Czarny', 'Apple iPhone 7 32GB Czarny', ' <header class="section-header"> <h3>Dane podstawowe</h3> </header> <table> <tbody> <tr> <th> Producent </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/p:Apple.htm">Apple</a> </li> </ul> </td> </tr> <tr> <th> System operacyjny </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/System_operacyjny:Apple_iOS.htm">Apple iOS</a> </li> </ul> </td> </tr> <tr> <th> Typ budowy </th> <td> <ul> <li class="attr-value"> Dotykowy </li> </ul> </td> </tr> <tr> <th> Pamięć wewnętrzna </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Pamiec_wewnetrzna:32_GB.htm">32 GB</a> </li> </ul> </td> </tr> <tr> <th> Pamięć RAM </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Pamiec_RAM:1_5_GB.htm">1,5 GB</a> </li> </ul> </td> </tr> <tr> <th> Dual SIM </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Dual_SIM:Nie.htm">Nie</a> </li> </ul> </td> </tr> <tr> <th> Rodzaj karty SIM </th> <td> <ul> <li class="attr-value"> nanoSIM </li> </ul> </td> </tr> <tr> <th> Rodzaj karty pamięci </th> <td> <ul> <li class="attr-value"> Nie obsługuje </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Ekran</h3> </header> <table> <tbody> <tr> <th> Przekątna ekranu </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Przekatna_ekranu:4_7_cala.htm">4,7 cala</a> </li> </ul> </td> </tr> <tr> <th> Technologia wyświetlacza </th> <td> <ul> <li class="attr-value"> IPS TFT </li> </ul> </td> </tr> <tr> <th> Rozdzielczość ekranu </th> <td> <ul> <li class="attr-value"> 750x1334 </li> </ul> </td> </tr> <tr> <th> Ilość kolor&#243;w wyświetlacza </th> <td> <ul> <li class="attr-value"> 16M </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Aparat</h3> </header> <table> <tbody> <tr> <th> Wbudowany aparat cyfrowy </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Wbudowany_aparat_cyfrowy:12_Mpix.htm">12 Mpix</a> </li> </ul> </td> </tr> <tr> <th> Lampa błyskowa </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Przedni aparat </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Łączność</h3> </header> <table> <tbody> <tr> <th> Moduł WiFi </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Hotspot WiFi </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Technologia LTE </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Technologia_LTE:Tak.htm">Tak</a> </li> </ul> </td> </tr> <tr> <th> NFC </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> GPS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> A-GPS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Bluetooth </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Miracast </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> DLNA </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> IrDA </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> USB </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Standard GSM </th> <td> <ul> <li class="attr-value"> 900 </li> <li class="attr-value"> 850 </li> <li class="attr-value"> 1800 </li> <li class="attr-value"> 1900 </li> </ul> </td> </tr> <tr> <th> Standard UMTS </th> <td> <ul> <li class="attr-value"> 850 </li> <li class="attr-value"> 900 </li> <li class="attr-value"> 1700 </li> <li class="attr-value"> 1900 </li> <li class="attr-value"> 2100 </li> </ul> </td> </tr> <tr> <th> GPRS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> EDGE </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> HSPA </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> HSPA+ </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> HSCSD </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Profil A2DP </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> WAP </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> xHTML </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Multimedia</h3> </header> <table> <tbody> <tr> <th> Java </th> <td> <ul> <li class="attr-value"> Nie obsługuje </li> </ul> </td> </tr> <tr> <th> Gry </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Radio </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Dyktafon </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Formaty audio </th> <td> <ul> <li class="attr-value"> AAC </li> <li class="attr-value"> WAV </li> <li class="attr-value"> AAX </li> <li class="attr-value"> AAX+ </li> <li class="attr-value"> AIFF </li> <li class="attr-value"> HE-AAC </li> </ul> </td> </tr> <tr> <th> Tuner DVB-H </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Wyjście TV </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Obsługa RSS </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Funkcje biznesowe</h3> </header> <table> <tbody> <tr> <th> Klient e-mail </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Protokoły e-mail </th> <td> <ul> <li class="attr-value"> POP3 </li> <li class="attr-value"> IMAP4 </li> <li class="attr-value"> SMTP </li> </ul> </td> </tr> <tr> <th> Kalendarz </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Organizer </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Kalkulator </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Konwerter walut </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Budzik </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Tryb samolotowy </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> SyncML </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Zasilanie</h3> </header> <table> <tbody> <tr> <th> Ładowanie bezprzewodowe </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Technologia baterii </th> <td> <ul> <li class="attr-value"> Li-Ion </li> </ul> </td> </tr> <tr> <th> Pojemność baterii </th> <td> <ul> <li class="attr-value"> 1960 mAh </li> </ul> </td> </tr> <tr> <th> Maksymalny czas czuwania </th> <td> <ul> <li class="attr-value"> 240 h </li> </ul> </td> </tr> <tr> <th> Maksymalny czas rozmowy </th> <td> <ul> <li class="attr-value"> 840 min </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Parametry fizyczne</h3> </header> <table> <tbody> <tr> <th> Kolor </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Kolor:Czarny.htm">Czarny</a> </li> </ul> </td> </tr> <tr> <th> Wymienna obudowa </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Wysokość </th> <td> <ul> <li class="attr-value"> 138,3 mm </li> </ul> </td> </tr> <tr> <th> Szerokość </th> <td> <ul> <li class="attr-value"> 67,1 mm </li> </ul> </td> </tr> <tr> <th> Grubość </th> <td> <ul> <li class="attr-value"> 7,10 mm </li> </ul> </td> </tr> <tr> <th> Waga </th> <td> <ul> <li class="attr-value"> 138 g </li> </ul> </td> </tr> <tr> <th> Czytnik linii papilarnych </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Procesor</h3> </header> <table> <tbody> <tr> <th> Model Procesora </th> <td> <ul> <li class="attr-value"> Apple A10 </li> </ul> </td> </tr> </tbody> </table>  <table> <tbody> <tr> <th> Popularne modele </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Smartfony/Popularne_modele:Apple_iPhone_7.htm">Apple iPhone 7</a> </li> </ul> </td> </tr> </tbody> </table> '),
(47053736, 'Smartwatche', 'Samsung', 'Gear S3 SM-R760 Frontier', 'Samsung Gear S3 SM-R760 Frontier', ' <header class="section-header"> <h3>Sieć</h3> </header> <table> <tbody> <tr> <th> Sieci </th> <td> <ul> <li class="attr-value"> Tylko Bluetooth </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Łączność</h3> </header> <table> <tbody> <tr> <th> ANT+ </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Technologia określania lokalizacji </th> <td> <ul> <li class="attr-value"> GPS, Glonass </li> </ul> </td> </tr> <tr> <th> Wi-Fi </th> <td> <ul> <li class="attr-value"> 802.11 b/g/n 2,4 GHz </li> </ul> </td> </tr> <tr> <th> NFC </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Wersja Bluetooth </th> <td> <ul> <li class="attr-value"> Bluetooth 4.2 </li> </ul> </td> </tr> <tr> <th> Profile Bluetooth </th> <td> <ul> <li class="attr-value"> A2DP, AVRCP, HFP, HSP </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Wyświetlacz</h3> </header> <table> <tbody> <tr> <th> Technologia wyświetlacza </th> <td> <ul> <li class="attr-value"> Super AMOLED </li> </ul> </td> </tr> <tr> <th> Wielkość wyświetlacza </th> <td> <ul> <li class="attr-value"> 1,3&quot; (32,9 mm) </li> </ul> </td> </tr> <tr> <th> Rozdzielczość wyświetlacza </th> <td> <ul> <li class="attr-value"> 360 x 360 </li> </ul> </td> </tr> <tr> <th> Głębia kolor&#243;w wyświetlacza </th> <td> <ul> <li class="attr-value"> 16M </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Procesor</h3> </header> <table> <tbody> <tr> <th> Taktowanie procesora </th> <td> <ul> <li class="attr-value"> 1 GHz </li> </ul> </td> </tr> <tr> <th> Typ procesora </th> <td> <ul> <li class="attr-value"> Dual Core </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Informacje og&#243;lne</h3> </header> <table> <tbody> <tr> <th> Kolor </th> <td> <ul> <li class="attr-value"> Space Grey </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Pamięć</h3> </header> <table> <tbody> <tr> <th> Wielkość RAM (GB) </th> <td> <ul> <li class="attr-value"> 0,75 GB </li> </ul> </td> </tr> <tr> <th> Wielkość ROM (GB) </th> <td> <ul> <li class="attr-value"> 4 GB </li> </ul> </td> </tr> <tr> <th> Dostępna pamięć (GB) </th> <td> <ul> <li class="attr-value"> 1,5 GB </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Parametry fizyczne</h3> </header> <table> <tbody> <tr> <th> Wymiary (WxSxG) </th> <td> <ul> <li class="attr-value"> 49 x 46 x 12,9 mm </li> </ul> </td> </tr> <tr> <th> Waga (g) </th> <td> <ul> <li class="attr-value"> 63 g </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Bateria</h3> </header> <table> <tbody> <tr> <th> Pojemność baterii </th> <td> <ul> <li class="attr-value"> 380 mAh </li> </ul> </td> </tr> <tr> <th> Bateria wyjmowana </th> <td> <ul> <li class="attr-value"> Nie </li> </ul> </td> </tr> <tr> <th> Typowy czas użytkowania </th> <td> <ul> <li class="attr-value"> 3 - 4 dni </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Audio i Wideo</h3> </header> <table> <tbody> <tr> <th> Format odtwarzania audio </th> <td> <ul> <li class="attr-value"> MP3, M4A, 3GA, AAC, OGG, OGA, WAV, WMA, AMR, AWB </li> </ul> </td> </tr> </tbody> </table>  <header class="section-header"> <h3>Usługi i Aplikacje</h3> </header> <table> <tbody> <tr> <th> S-Voice </th> <td> <ul> <li class="attr-value"> Tak </li> </ul> </td> </tr> <tr> <th> Funkcje dodatkowe </th> <td> <ul> <li class="attr-value"> Wiadomości, Telefon, Kontakty, S Health, Pogoda, Odtwarzacz muzyki, S Voice, Ustawienia, Przypomnienia, Wysokościomierz/Barometr, Grafik, Budzik, Czas na świecie, News Briefing, Galeria, Znajdź m&#243;j telefon, Email, Monster Vampire </li> </ul> </td> </tr> </tbody> </table> '),
(48946775, 'Monitory pracy serca, pulsometry', 'Polar', 'M200 Czerwony (90061217)', 'Polar M200 Czerwony (90061217)', ' <table> <tbody> <tr> <th> Producent </th> <td> <ul> <li class="attr-value"> <a class="dotted-link" href="/Monitory_pracy_serca,_pulsometry/p:Polar.htm">Polar</a> </li> </ul> </td> </tr> </tbody> </table> ');

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `opinions`
--
ALTER TABLE `opinions`
  ADD CONSTRAINT `prdFK` FOREIGN KEY (`prd_id`) REFERENCES `products` (`prd_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
