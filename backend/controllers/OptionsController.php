<?php

class OptionsController extends ETL_FrontendController
{

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
        $this->isCmsLogged();
        $this->view->menu = 'options';
    }

    public function indexAction()
    {
        // action body

        $options = new Model_DbTable_Options();
        $optionsList = $options->fetchAll();
        $this->view->list = $optionsList;
    }

}

