<?php

class ProductController extends ETL_FrontendController
{

    public function init()
    {
        /* Initialize action controller here */
        parent::init();
        $this->isCmsLogged();
        $this->view->menu = 'produkty';


        $config = $this->view->config = Zend_Registry::get('APPCONFIG');

    }

    public function indexAction()
    {
        // action body

        $model = new Model_DbTable_Products();
        $products = $model->getAllProducts();
        $this->view->list = $products;
    }

    public function productAction()
    {
        // action body

        $model = new Model_DbTable_Products();

        $id = $this->getParam('id',0);
        if ($id == 0) throw new Zend_Exception('BŁĄD',404);

        $obj = $model->find($id)->current();
        if (!$obj) {
            throw new Zend_Controller_Action_Exception('Błąd - brak elementu',404);
        }

        $this->view->product = $obj;

        $opinionModel = new Model_DbTable_Opinions();
        $this->view->opinions = $opinionModel->getForProduct($obj->prd_id);

    }


    /* outputs csv */

    public function commentsAction() {
        $id = $this->getParam('id',0);
        $prd_id = $this->getParam('prd_id',0);

        $opinionModel = new Model_DbTable_Opinions();
        if ($id == 0)  {
            //get all
            $opinions = $opinionModel->getForProduct($prd_id);
            $opinions = $opinions->toArray();
        } else {
            $opinions = array($opinionModel->find($id)->current()->toArray());
        }
        $csv = array(
            'opn_id'=> 'opn_id',
            'text'=> 'text',
            'date'=> 'date',
            'author'=> 'author',
            'possesion'=> 'possesion',
            'rate'=> 'rate',
            'recommend'=> 'recommend',
            'advantage'=> 'advantage',
            'disadvantage'=> 'disadvantage',
            'vote_yes'=> 'vote_yes',
            'vote_no'=> 'vote_no',
            'comment_id'=> 'comment_id',
            'prd_id'=> 'prd_id',
        );

        $fullCSV = array_merge(array($csv),$opinions);

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="opn.csv";');

        $f = fopen('php://output', 'w');
        fprintf($f, chr(0xEF).chr(0xBB).chr(0xBF)); //utf-8
        foreach ($list as $line) {
            fputcsv($f, $line, ',', '"');
        }
        exit;



    }

    public function deleteAction() {
        $prd_id = (int) $this->getParam('prd_id',0);

        $opinionModel = new Model_DbTable_Opinions();
        $opinionModel->deleteOpinion($prd_id);
        return $this-> _helper -> redirector ('index', 'product', 'default');

    }

}

