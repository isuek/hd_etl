<?php

class IndexController extends ETL_FrontendController
{

    protected $sessionData = null;

    public function init()
    {
        parent::init();
        $this->isCmsLogged();
        $this->view->menu = 'index';

        $this->sessionData = new Zend_Session_Namespace('etl');

    }

    /**
     * Akcja po zalogowaniu
     */
    public function indexAction()
    {

    }


    /**
     * ---------------------------------Akcja: extract---------------------------------
     * odpowiedzialna za pobranie danych z serwisu ceneo.pl
     */
    public function extractAction()
    {
        $idParam = $this->getParam('productid',0);
        $id = (int) $idParam;

        if ($id == 0) {
            $msg = 'Nie wprowadzono Id produktu';
            if (strlen($idParam) > 1) {
                $msg = 'Wprowadzone Id:' . $idParam . ' jest nieprawidłowe.';
            }
            return $this->_helper->json(array('status'=>'ERROR', 'msg'=> $msg));
        }

        $this->extract($id);
    }

    /**
     * Pobiera dane o produkcie z serwisu ceneo.pl i zapisuje wynik do sesji.
     */

    protected function extract($id, $etl = false){
        $idCeneo = $id;
        $this->sessionData->ceneoID = $idCeneo;

        $this->getFromCeneo($idCeneo);
        $prd = $this->sessionData->product;
        $result = array('commentCount' => $this->sessionData->commentCount, 'product' => $prd );
        if (!$etl) {
            if (!empty($prd['specs'])) {
                return $this->_helper->json(array_merge(array('status'=>'OK'),$result));
            } else {
                $msg = 'Nie znaleziono produktu, upewnij się że Id jest prawidłowe';
                return $this->_helper->json(array('status'=>'ERROR', 'data'=>$result, 'msg'=> $msg));
            }
        } else {
            return $result;
        }
    }

    /**
     * Pobiera dane produktu z ceneo
     */
    protected function getFromCeneo($idCeneo) {
        $html = new simple_html_dom();
        $rawCeneo = my_curl('http://www.ceneo.pl/' . $idCeneo, array());
        $html->load($rawCeneo);

        if (empty($rawCeneo))  {
            return false;
        }

        $productData = array();
        $type = $html->find('.breadcrumbs .breadcrumb a span', -1);
        if (!empty($type)) {
            $productData['type'] = $type->innertext;
        }

        $name = $html->find('h1.product-name', 0);
        if (!empty($name)) {
            $name = $name->innertext;
            $firstSpace = strpos($name, ' ');
            $brand = trim(substr($name, 0, $firstSpace));
            $model = trim(substr($name, $firstSpace));

            $productData['brand'] = $brand;
            $productData['model'] = $model;
            $productData['name'] =  $name;
        }


        $specs = $html->find('#productTechSpecs .specs-group');
        $specsPlain = '';
        foreach ($specs as $s) {
            $specsPlain .= preg_replace('/\s\s+/', ' ', $s->innertext);
        }
        $productData['specs'] = $specsPlain;
        $this->sessionData->product = $productData;

        $count = $html->find('.product-reviews-link span', -1);
        if (!empty($count)) {
            $count = $count->innertext;
            $pages = floor($count / 10) + 1;
            $this->sessionData->commentCount = (int)$count;
            $this->sessionData->ceneoPages = $pages;
        }

         return $this->getCeneoComments();
    }

    /**
     * Pobiera opinie z ceneo, zapisuje w sesji wynik
     */

    protected function getCeneoComments() {
        $ceneoData = array();
        for ($i = 1; $i <= $this->sessionData->ceneoPages; $i++) {
            array_push($ceneoData, $this->getCommentsForPage($i));
        }
        $this->sessionData->ceneoRaw = $ceneoData;

        return true;
    }

    /**
     * Pobieranie opini curl'em dla każdej podstrony
     */

    protected function getCommentsForPage($pageNr) {
        $urlRawData = my_curl('http://www.ceneo.pl/' . $this->sessionData->ceneoID . '/opinie-' . $pageNr, array(), 4);
        return $urlRawData;
    }



    /**
     * ---------------------------------Akcja: transfor---------------------------------
     * odpowiedzialna za przekształcenie pobranych danych z serwisu ceneo.
     */

    public function transformAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->transfromCeneo();
    }

    /**
     * Przetwarza przetwarzanie wcześniej pobranych danych  z serwisu
     */

    protected function transfromCeneo($etl = false) {
        $data = $this->sessionData->ceneoRaw;
        $j = 0;
        $commentData = array();
        $averageRating = 0;
        $possesionCount = 0;
        $positiveReference = 0;
        $negativeReference = 0;

        foreach ($data as $html) {
            $d = new simple_html_dom();
            $d->load($html);
            $comments = $d->find('.product-reviews .product-review');

            // iteracja po komentarzach
            foreach ($comments as $cnt) {
                // pobieram autora
                $author = $cnt->find('.product-reviewer', 0);
                if (!empty($author)) $commentData[$j]['author'] = trim($author->innertext);

                // pobieram treść komentarza
                $text = $cnt->find('.product-review-body',0);
                if (!empty($text)) { $commentData[$j]['text'] = trim($text->innertext); }

                // pobieram zalety produktu
                $advantage = $cnt->find('.pros-cell ul', 0);
                if (!empty($advantage)) {
                    $plain  = str_replace(array('<li>','</li>'), array('','<br />'),strip_tags($advantage->innertext, '<li>'));
                    $plain = rtrim(trim($plain),'<br />');

                    $commentData[$j]['advantage'] = preg_replace('/\s\s+/', ' ', $plain);
                }

                // pobieram wady produktu
                $disadvantage = $cnt->find('.cons-cell ul', 0);
                if (!empty($disadvantage)) {
                    $plain  = str_replace(array('<li>','</li>'), array('','<br />'),strip_tags($disadvantage->innertext, '<li>'));
                    $plain = rtrim(trim($plain),'<br />');
                    $commentData[$j]['disadvantage'] = preg_replace('/\s\s+/', ' ', $plain);
                }

                //pobieram datę
                $date = $cnt->find('.review-time time', 0);
                if (!empty($date)) $commentData[$j]['date'] = trim($date->datetime);

                //pobieram ocene
                $rate = $cnt->find('.review-score-count', 0);
                if (!empty($rate)) {
                    $ratearr = explode('/',$rate->innertext);
                    $ratetext = array_shift($ratearr);
                    $rate = str_replace(',','.',$ratetext);
                    $commentData[$j]['rate'] = $rate;
                    $averageRating += floatval($rate);
                }

                //sprawdzam posiadanie produktu
                $possesion = $cnt->find('.product-review-pz', 0);
                if (!empty($possesion)) {
                    $commentData[$j]['possesion'] = true;
                    $possesionCount++;
                } else {
                    $commentData[$j]['possesion'] = false;
                }

                //pobieram rekomendację POLECAM / NIE POLECAM
                $recommend = $cnt->find('.product-review-summary .product-recommended', 0);
                $notRecommend = $cnt->find('.product-review-summary .product-not-recommended', 0);
                if (!empty($recommend)) {
                    $commentData[$j]['recommend'] = $recommend->innertext;
                    $positiveReference++;
                }
                else if (!empty($notRecommend)) {
                    $commentData[$j]['recommend'] = $notRecommend->innertext;
                    $negativeReference++;
                }

                //Pobieram id komentarza
                $commentId = $cnt->find('.product-review-comment-dummy', 0);

                if (!empty($commentId)) {
                    $commentId = $commentId->attr['data-review-id'];
                    $commentData[$j]['comment_id'] = (int)$commentId;
                    //pobieram ilość łapek w górę (like) i w dół (unlike)
                    $voteYes = $cnt->find('#votes-yes-' . $commentId, 0);
                    $voteNo = $cnt->find('#votes-no-' . $commentId, 0);
                    if (!empty($voteYes) && !empty($voteNo)) {
                        $commentData[$j]['vote_yes'] = (int)($voteYes->innertext);
                        $commentData[$j]['vote_no'] = (int)($voteNo->innertext);

                    } else {
                        $commentData[$j]['vote_yes'] = 0;
                        $commentData[$j]['vote_no'] = 0;
                    }
                } else {
                    $commentData[$j]['comment_id'] = 0;
                    $commentData[$j]['vote_yes'] = 0;
                    $commentData[$j]['vote_no'] = 0;
                }

                $commentData[$j]['prd_id'] = $this->sessionData->ceneoID;

                $j++;

                unset($text, $advantage, $disadvantage, $date, $author, $rate, $possesion, $recommend, $voteYes, $voteNo, $commentId);
            }
            unset($d);
        }
        if ($negativeReference > $positiveReference) {
            $finallyReference = "<p style='color:red;'>nie polecany</p>";
        } else {
            $finallyReference = "<p style='color:green;'>polecany</p>";
        }

        $averageRating = round($averageRating/count($commentData), 2);
        $this->sessionData->ceneo = $commentData;
        $result = array('commentsCount' => count($commentData), 'averageRating' => $averageRating, 'finallyReference' => $finallyReference, 'possesionCount'=>$possesionCount);
        if (!$etl) {
            return $this->_helper->json(array_merge(array('status'=>'OK'),$result));
        } else {
            return $result;
        }
    }



    /**
     * ---------------------------------Akcja: load---------------------------------
     * odpowiedzialna za wrzucanie przekształconych danych do bazy danych.
     */

    public function loadDataAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $this->loadData();
    }


    /**
     * @return mixed
     * Zapisuje przetworzone dane
     */


    protected function loadData($etl = false) {
        $ceneoID =  $this->sessionData->ceneoID;
        $productData = $this->sessionData->product;
        $ceneoComments = $this->sessionData->ceneo;

        $productModel = new Model_DbTable_Products();
        $productModel->loadDataProduct($ceneoID,$productData);

        $opinionsModel = new Model_DbTable_Opinions();

        $loadResult = $opinionsModel->saveCeneoData($ceneoComments);

        $loadResult['url'] = '/product/product/id/'.$ceneoID;

        Zend_Session::namespaceUnset('etl');
        if (!$etl) {
            return $this->_helper->json(array_merge(array('status'=>'OK'),$loadResult));
        } else {
            return $loadResult;
        }
    }



    /**
     * Akcja podejrzenia danych tymczasowych
     */

    public function resultAction() {
        $this->view->sess = $this->sessionData;
    }

    /**
     * Pełny proces etl
     */
    public function etlAction(){
        $id = (int) $this->getParam('productid',0);
        if ($id == 0)   return $this->_helper->json(array('status'=>'ERROR', 'msg'=>'Nie wprowadzono Id produktu'));

        $extractResult = $this->extract($id,true);
        $transformResult = $this->transfromCeneo(true);
        $loadResult = $this->loadData(true);
        $resultETL = array('extract'=> $extractResult, 'transform'=>$transformResult, 'loadData'=> $loadResult);
        return $this->_helper->json(array_merge(array('status'=>'OK'),$resultETL));
    }

}
