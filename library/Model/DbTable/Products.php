<?php

class Model_DbTable_Products extends Zend_Db_Table_Abstract
{

    protected $_name = 'products';


    /**
     * Końcowy etap procesu "load". Wrzuca dane o producie do bazy danych
     */

    public function loadDataProduct($id,$data) {
        // check if exists
        $obj =  $this->find($id);
        $count = $obj->count();
        $data['prd_id'] = $id;
        if ($count == 0) {
            $this->insert($data);
        } else {
            $obj = $obj->current();
            $obj->setFromArray($data);
            $obj->save();
        }

    }

    /**
     * Zwraca wszystkie produkty
     */
    public function getAllProducts() {
        return $this->fetchAll();
    }
}

