<?php

class Model_DbTable_Opinions extends Zend_Db_Table_Abstract
{

    protected $_name = 'opinions';
    protected $_rowClass = 'Model_Opinion';


    /**
     * Zapisuje nowe rekordy porównując dane zapisane z pobranymi z serwisu ceneo.pl
     */

    public function saveCeneoData($data) {
        $commentExists = 0;
        $addedComment = 0;

        foreach ($data as $d) {
            // Sprawdzam czy mamy już taki komentarz w bazie
            $select = $this->select()
                ->where('comment_id = (?)',$d['comment_id'])
                ->where('prd_id = (?)',$d['prd_id']);
            $existing =  $this->fetchAll($select);
            if (count($existing) == 0) {
                $this->insert($d);
                $addedComment++;
            } else {
                $commentExists++;
            }

        }
         return array('commentExists' => $commentExists, 'addedComment' => $addedComment);
    }

    /**
     * Pobiera opinie dla danego produktu
     */

    public function getForProduct($prd_id) {
        return $this->fetchAll($this->select()->where('prd_id = ?', $prd_id ));
    }

    public function deleteOpinion($prd_id) {
        return $this->delete('prd_id=' . $prd_id);
    }

}

