<?php

class Model_DbTable_Options extends Zend_Db_Table_Abstract
{

    protected $_name = 'options';
    protected $_rowClass = 'Model_Options';


    public  function getMailConfig() {
        $select = $this->select()->where('opt_namespace = "mail"');

        return $this->fetchAll($select);
    }


}

