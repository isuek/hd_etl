<?php

class ETL_NewPasswordForm extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */


        $this -> addElement('password', 'usr_password', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'placeholder' => 'Hasło',
            'class' => 'form-control'

        ));
        $this->usr_password ->addDecorator('Errors');
        $this -> usr_password ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));


        $this -> addElement('password', 'usr_password2', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'placeholder' => 'Powtórz Hasło',
            'class' => 'form-control'

        ));
        $this->usr_password2 ->addDecorator('Errors');
        $this -> usr_password2 ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));
        $this -> usr_password2 -> addValidator(new ETL_Valid_PasswordMatch());



        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'value'    => 'Wyślij ',
            'label'    => 'Wyślij',
            'class'     => 'btn btn-primary  btn-flat'

        ));



        $this->setElementDecorators(array(
            'ViewHelper',
            'Errors',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class'=> 'form-group has-feedback')),
//            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form'))
        ));

        $element = $this->getElement('submit');
        $element->removeDecorator('label');


    }


}

