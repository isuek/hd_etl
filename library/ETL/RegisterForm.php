<?php

class ETL_RegisterForm extends Zend_Form
{


    public function init()
    {
        /* Form Elements & Other Definitions Here ... */


        $this -> addElement('text', 'usr_email', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Email',
            'class' => 'validate[required]'

        ));
        $this->usr_email ->addDecorator('Errors');
        $this -> usr_email ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));
        $this -> usr_email -> addValidator(new ETL_Valid_EmailExists('Model_DbTable_Users'));


        $this -> addElement('password', 'usr_password', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Hasło',
            'class' => 'validate[required]'

        ));
        $this->usr_password ->addDecorator('Errors');
        $this -> usr_password ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));


        $this -> addElement('password', 'usr_password2', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Powtórz Hasło',
            'class' => 'validate[required]'

        ));
        $this->usr_password2 ->addDecorator('Errors');
        $this -> usr_password2 ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));
        $this -> usr_password2 -> addValidator(new ETL_Valid_PasswordMatch());


        $this -> addElement('text', 'usr_name', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Imię',
            'class' => 'validate[required]'

        ));
        $this -> usr_name ->addDecorator('Errors');
        $this -> usr_name ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));

        $this -> addElement('text', 'usr_lastname', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Nazwisko',
            'class' => 'validate[required]'

        ));
        $this -> usr_lastname ->addDecorator('Errors');
        $this -> usr_lastname ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));

        $this -> addElement('text', 'usr_phone', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Telefon',
            'class' => 'validate[required]'

        ));
        $this -> usr_phone ->addDecorator('Errors');
        $this -> usr_phone ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));

        $this -> addElement('text', 'usr_address', array(
            'filters' => array('StringTrim','StripTags'),
            'label' => 'Adres',
        ));

        $this -> addElement('text', 'usr_zip', array(
            'filters' => array('StringTrim','StripTags'),
            'label' => 'Kod pocztowy',
        ));

        $this -> addElement('text', 'usr_city', array(
            'filters' => array('StringTrim','StripTags'),
            'label' => 'Miasto',
        ));



        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'value'    => 'Rejestruj ',
            'label'    => 'Rejestruj',
            'class'     => 'submit'
        ));



        $this->setElementDecorators(array(
            'ViewHelper',
            'Errors',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class'=> 'col_10 inputcont')),
            array('Label', array('tag' => 'div', 'tagClass' => 'col_2' )),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'formrow clearfix'))
        ));

        $element = $this->getElement('submit');
        $element->removeDecorator('label');

    }


}

