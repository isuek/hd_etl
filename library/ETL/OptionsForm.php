<?php

class ETL_OptionsForm extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */


        $this -> addElement('text', 'opt_name', array(
            'required' => true,
            'filters' => array('StringTrim','StripTags'),
            'validators'=> array(
                array('NotEmpty',true)),
            'label' => 'Nazwa',
            'class' => ' form-control disabled',
            'readonly'=>'readonly'
        ));
        $this->opt_name ->addDecorator('Errors');
        $this -> opt_name ->getValidator('NotEmpty') -> setMessages(array(
            Zend_Validate_NotEmpty::IS_EMPTY => "to pole nie może być puste"
        ));


        $this -> addElement('textarea', 'opt_value', array(
            'filters' => array('StringTrim','StripTags'),
            'label' => 'Wartość',
            'class' => 'form-control'
        ));
        $this->opt_value ->addDecorator('Errors');





        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'value'    => 'Zapisz ',
            'label'    => 'Zapisz',
            'class'     => 'btn btn-primary  btn-fla'

        ));


        $this -> addElement('hidden', 'opt_image', array(
            'filters' => array('StringTrim','StripTags'),
            'class' => 'imageOptions'
        ));



        $this->setElementDecorators(array(
            'ViewHelper',
            'Errors',
            array(array('data' => 'HtmlTag'), array('tag' => 'div', 'class'=> 'col_10 inputcont')),
            array('Label', array('tag' => 'label', 'tagClass' => 'col_2' )),
            array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
        ));

        $element = $this->getElement('submit');
        $element->removeDecorator('label');

    }


}

