<?php
class ETL_Common {



    public static function sendMail($to,$subject,$text){

        if (empty($to)) return false;
        $serwer =''; $user=''; $pass=''; $from='';

        $options = new Model_DbTable_Options();
        $conf = $options->getMailConfig();


        foreach ($conf as $option) {
            if ($option['opt_key']=='mail_smtp') $serwer = $option['opt_value'];
            if ($option['opt_key']=='mail_username')   $user = $option['opt_value'];
            if ($option['opt_key']=='mail_password')   $pass = $option['opt_value'];
            if ($option['opt_key']=='mail_username')   $from = $option['opt_value'];
        }



        $config = array (
            'port' => 587,
            'ssl' => 'tls',
            'auth' => 'login',
            'username' => $user,
            'password' => $pass
        );


        $transport = new Zend_Mail_Transport_Smtp($serwer,$config);
        Zend_Mail::setDefaultTransport($transport);

        $mail = new Zend_Mail('UTF-8');
        $mail->addTo($to);
        $mail->setFrom($from,'');
        $mail->setSubject($subject);
        $mail->setReplyTo($from);
        $mail->setBodyText($text);
        $mail->send();


    }


    public static function sendMailAttachment($to,$subject,$text,$filepath){

        if (empty($to)) return false;
        $serwer =''; $user=''; $pass=''; $from='';

        $options = new Model_DbTable_Options();
        $conf = $options->getMailConfig();


        foreach ($conf as $option) {
            if ($option['opt_key']=='mail_smtp') $serwer = $option['opt_value'];
            if ($option['opt_key']=='mail_username')   $user = $option['opt_value'];
            if ($option['opt_key']=='mail_password')   $pass = $option['opt_value'];
            if ($option['opt_key']=='mail_username')   $from = $option['opt_value'];
        }

        $config = array (
            'port' => 587,
            'ssl' => 'tls',
            'auth' => 'login',
            'username' => $user,
            'password' => $pass
        );


        $transport = new Zend_Mail_Transport_Smtp($serwer,$config);
        Zend_Mail::setDefaultTransport($transport);

        $mail = new Zend_Mail('UTF-8');
        $mail->addTo($to);
        $mail->setFrom($from,'');
        $mail->setSubject($subject);
        $mail->setReplyTo($from);
        $mail->setBodyText($text);

        // attach file
        $mail->setType(Zend_Mime::MULTIPART_RELATED);

        $at  = $mail->createAttachment(file_get_contents($filepath));
        $at->filename = basename($filepath);
        $at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
        $at->encoding = Zend_Mime::ENCODING_BASE64;


        if (APPLICATION_ENV == 'development') {
            if ($to == 'etl@etl.pl') {
                $mail->send();
            }
        } else {
            $mail->send();
        }



    }


    public static function generateRandomPassword() {
        //Initialize the random password
        $password = '';

        //Initialize a random desired length
        $desired_length = rand(8, 10);

        for($length = 0; $length < $desired_length; $length++) {
            //Append a random ASCII character (including symbols)
            $password .= chr(rand(32, 126));
        }

        return $password;
    }

    private static function http_build_query($data, $prefix='', $sep='', $key='') {
        $ret = array();
        foreach ((array)$data as $k => $v) {
            if (is_int($k) && $prefix != null) {
                $k = urlencode($prefix . $k);
            }
            if ((!empty($key)) || ($key === 0))  $k = $key.'['.urlencode($k).']';
            if (is_array($v) || is_object($v)) {
                array_push($ret, http_build_query($v, '', $sep, $k));
            } else {
                array_push($ret, $k.'='.urlencode($v));
            }
        }
        if (empty($sep)) $sep = ini_get('arg_separator.output');
        return implode($sep, $ret);
    }








}