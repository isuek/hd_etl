<?php

class ETL_Valid_EmailExists extends Zend_Validate_Abstract
{

    const EXISTS = 'exists';

    protected $_messageTemplates = array(
        self::EXISTS => 'Podany e-mail znajduje się już w naszej bazie, spróbuj inny'
    );

    private $model;
    private $id;

    public function __construct($modelName,$id=null)
    {
        if (!empty($modelName)) {
            $this->model = new $modelName;
        }

        if (!empty($id)) {
            $this->id = $id;
        }

    }


    public function isValid($value, $context = null)
    {

        if (empty($this->id)) {
            if ($this->model->mailExists($value)) {
                $this->_error(self::EXISTS);
                return false;
            } else {
                return true;
            }
        } else {
            if ($this->model->mailExists($value,$this->id)) {
                $this->_error(self::EXISTS);
                return false;
            } else {
                return true;
            }
        }

    }

}