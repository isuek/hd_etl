<?php

class ETL_Valid_PasswordMatch extends Zend_Validate_Abstract {

    const NOT_MATCH = 'NotMatch';

    protected $_messageTemplates = array(
        self::NOT_MATCH => 'Podane hasła nie są indentyczne'
    );


    public function isValid($value, $context=null) {

        $pwd1 =$value;
        $pwd2 = $context['usr_password'];

        if ($pwd1==$pwd2) return true; else {

            $this->_error(self::NOT_MATCH);
            return false;
        }





    }

}