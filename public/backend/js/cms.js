var cms = {

    mainDomain: '',
    ajaxLoading: false,

    init:function() {

        this.initTables();
        this.initConfirm();
        this.initDatePickers();
        this.initModals();

        this.actionForm();
    },

    actionForm: function() {
        var errorAction = function (data, msg) {
            console.log('jestem');
            var errorMsg = msg || "Status: " + data.status + " - " + data.statusText;
            $('#errorData').text(errorMsg);
            $('.modal.error').modal('show');  cms.ajaxLoading = false;
            $('#errorData').html(data.msg);
            $('#overlay').hide();
        };

        var showExtractData = function (data) {
            $('#commentCount').text(data.commentCount);
            $('#product').text(data.product.name);
            $('#type').text(data.product.type);
            $('#specs').html(data.product.specs);
            $('#extractData').show();
            $('.transformTrigger').removeClass('disabled');
            $('.loadDataTrigger').addClass('disabled');
            $('#overlay').hide();
        };

        var showTransformData = function (data) {
            $('#ceneoCount').text(data.commentsCount);
            $('#averageRating').text(data.averageRating);
            $('#possesionCount').text(data.possesionCount);
            $('#finallyReference').html(data.finallyReference);
            $('#transformData').show();
            $('.transformTrigger').addClass('disabled');
            $('.saveDataTrigger').removeClass('disabled');
            $('#overlay').hide();
        };

        var showLoadData = function (data) {
            $('#newCount').text(data.addedComment);
            $('#untouchedCount').text(data.commentExists);
            $('#productLink').attr('href',data.url);
            $('#loadData').show();
            $('.saveDataTrigger').addClass('disabled');
            $('#overlay').hide();
        };

        $('.loadForm').on('submit',function(e) {
            e.preventDefault();
            e.stopPropagation();
            var form = $(this);
            if (cms.ajaxLoading) return;
            cms.ajaxLoading = true;
            $('#overlay').show();
            $.ajax({
                url: '/index/extract',
                data: form.serialize(),
                method: "POST",
                success: function(data){
                    if (data.status == 'OK') {
                        console.log(data);
                        showExtractData(data);
                    } else {
                        console.log()
                        if (form.serialize() != 'productid=') {
                            errorAction(data, 'Nie znaleziono produktu, upewnij się że Id jest prawidłowe');
                        }
                    }
                    cms.ajaxLoading = false;
                },
                error: function (data) {
                console.log(data)
                    if (data.status = 200 && form.serialize() != 'productid=') {
                        errorAction(data, "Produkt o wprowadzonym Id nie istnieje");
                        $('#errorData').html(data.responseText);
                    } else {
                         errorAction(data);
                    }
                    cms.ajaxLoading = false;
                }
            });
        });

        $('.transformTrigger').on('click',function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (cms.ajaxLoading || $(this).hasClass('disabled')) return;
            cms.ajaxLoading = true;
            $('#overlay').show();
            $.ajax({
                url: '/index/transform',
                method: "POST",
                success: function(data){
                    if (data.status == 'OK') {
                        console.log(data);
                        showTransformData(data);
                    } else {
                        errorAction(data);
                    }
                    cms.ajaxLoading = false;
                },
                error: function (data) {
                    errorAction(data);
                    cms.ajaxLoading = false;
                    $('#overlay').hide();
                }
            });
        });



        $('.saveDataTrigger').on('click',function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (cms.ajaxLoading || $(this).hasClass('disabled')) return;
            cms.ajaxLoading = true;
            $('#overlay').show();
            $.ajax({
                url: '/index/load-data',
                method: "POST",
                success: function(data){
                    if (data.status == 'OK') {
                        console.log(data);
                        showLoadData(data);
                    } else {
                        errorAction(data);
                    }
                    cms.ajaxLoading = false;
                },
                error: function (data) {
                    errorAction(data);
                    cms.ajaxLoading = false;
                }
            });
        });

        $('.etlTrigger').on('click',function(e) {
            e.preventDefault();
            e.stopPropagation();
            if (cms.ajaxLoading || $(this).hasClass('disabled')) return;
            cms.ajaxLoading = true;
            $('#overlay').show();
            $.ajax({
                url: '/index/etl',
                data: { productid: $('[name=productid]').val()},
                method: "POST",
                success: function(data){
                    if (data.status == 'OK') {
                        console.log(data);
                        showExtractData(data.extract);
                        showTransformData(data.transform);
                        showLoadData(data.loadData);
                    } else {
                        errorAction(data);
                        $('#overlay').hide();
                    }
                    cms.ajaxLoading = false;
                },
                error: function (data) {
                    console.log("etlTrigger - > error", data)
                    errorAction(data);
                    cms.ajaxLoading = false;
                    $('#overlay').hide();
                }
            });
        });


    },



    initModals: function() {
        $('.ajaxModal').on('click',function(e){
            e.preventDefault();
            $('.modal').remove();
            if (!cms.ajaxLoading) {
                var href = $(this).attr('href');
                cms.ajaxLoading = true;
                $.ajax(href,{
                    method: "POST",
                    success: function(data){
                        cms.ajaxLoading = false;
                        $('body').append($(data));
                        $('.modal').modal('show');
                    }
                })
            }
        });
    },

    initConfirm: function() {
        $('.confirmAction').on('click',function(e){
            if (!confirm('Czy jesteś pewien?')) e.preventDefault();
        });

    },

    initDatePickers: function() {
        $('.datepicker').datepicker({
                orientation: 'top',
                autoclose: true,
                format: 'yyyy-mm-dd'
            });

    },

    initTables: function() {
        $(".customTable").DataTable({
            "paging": false,
            info: false
        });
    }




};